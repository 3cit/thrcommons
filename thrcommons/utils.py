from string import ascii_letters, digits
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from logging import getLogger
from random import choice
from threading import Thread

logger = getLogger('thrcommons.utils')


class EmailSendingThread(Thread):
    """
    Utilitário para envio de e-mails.

    TODO: Criar uma Queue class para gerenciamento da fila de e-mails, quando vários.

    Uso:
        EmailSendingThread(
            recipient_list=['lista', 'de', 'destinatários',],
            subject_text='assunto da mensagem', # opcional (1)
            message_text='texto plano da mensagem', # (2)
            subject_text_file_name=None, # (1)
            message_text_file_name=None, # (2)
            html_template_name=None,
            context_data=None,
            files=['path/to/dir/file.ext'] or ((b'type', 'mimetype'),)
            *args,
            **kwargs)

        Observações:
            (1) - Obrigatório a informação de um dos dois campos
            (2) - Obrigatória a informação de pelo menos um dos campos
    """

    def __init__(self, recipient_list, subject_text=None, message_text=None, subject_text_file_name=None,
                 message_text_file_name=None, html_template_name=None, context_data=None, *args,
                 **kwargs):

        if 'from_email' in kwargs:
            from_email = kwargs.pop('from_email')
            if 'from_email_owner' in kwargs:
                from_email_owner = kwargs.pop('from_email_owner')
                self.from_email = '{owner} <{email_address}>'.format(owner=from_email_owner,
                                                                     email_address=from_email)
            else:
                self.from_email = from_email
        else:
            self.from_email = settings.DEFAULT_FROM_EMAIL
        logger.info(
            'Configurado para enviar a partir do e-mail {}'.format(self.from_email))

        if type(recipient_list) not in [tuple, list]:
            self.recipient_list = (recipient_list,)
        else:
            self.recipient_list = recipient_list

        # Assunto da mensagem
        if subject_text is None and subject_text_file_name is not None:
            self.subject_text = loader.render_to_string(subject_text_file_name,
                                                        context_data or {})
            self.subject_text = ''.join(self.subject_text.splitlines())
        elif subject_text is not None:
            self.subject_text = subject_text
        else:
            raise Exception(
                'Nem nem nome do template nem texto de assunto foram providos.')

        # Texto da mensagem
        if message_text is None and message_text_file_name is not None:
            self.message_text = loader.render_to_string(message_text_file_name,
                                                        context_data or {})
        elif message_text is not None:
            self.message_text = message_text
        # HTML da mensagem

        if html_template_name is not None:
            self.html_template = loader.render_to_string(html_template_name,
                                                         context_data or {})

        if not hasattr(self, 'message_text') and not hasattr(self, 'html_template'):
            raise Exception('Mensagem sem corpo, nem texto, nem HTML.')

        super().__init__(*args, **kwargs)

    def run(self):
        "Envia o(s) e-mail(s) e registra o resultado no log"
        mail_dict = dict(
            subject=self.subject_text,
            to=self.recipient_list,
            from_email=self.from_email,
            body=self.message_text)

        mailer = EmailMultiAlternatives(**mail_dict)

        if hasattr(self, 'html_template'):
            mailer.attach_alternative(self.html_template, 'text/html')

        sent_mail = mailer.send(fail_silently=False)

        # Apenas para fins de debug, relata se o e-mail foi enviado ou não
        destinatario_s = ", ".join(self.recipient_list) if len(
            self.recipient_list) > 1 else self.recipient_list[0]
        if sent_mail:
            logger.debug("E-mail enviado para {}.".format(destinatario_s))
        else:
            logger.error('Falha ao enviar email para {}.'.format(
                destinatario_s))


def gen_random_code(num_chars):
    codigo = ''.join(choice(ascii_letters + digits)
                     for _ in range(num_chars))
    return codigo
