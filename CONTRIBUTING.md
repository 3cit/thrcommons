# Contribuindo com este projeto

Sinta-se livre para editar este projeto, que ainda se encontra em fase inicial.

## Compilando pacote e testando

Para testar o uso deste pacote em outros projetos, basta executar `python setup.py sdist` para gerar o código-fonte com os módulos e em seguida instalar o pacote .tar.gz gerado através do comando install do pip. Daí é só importar e partir para o abraço.
