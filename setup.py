import os
from setuptools import find_packages, setup

with open("README.md", 'r') as readme:
    long_description = readme.read()  # hehe...

# Read more at: https://docs.djangoproject.com/en/2.2/intro/reusable-apps/
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(name='thrcommons',
      author='3cIT',
      author_email='3cit.sd@gmail.com',
      version='0.1.3',
      description='Some stuff that are used in other 3cIT projects.',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://bitbucket.org/3cit/thrcommons/",
      packages=find_packages(),
      install_requires=['django-import-export'],
      classifiers=('Development Status :: 3 - Alpha',
                   'Programming Language :: Python :: 3',
                   'Framework :: Django :: 2.2',
                   'Natural Language :: Portuguese (Brazilian)',))
