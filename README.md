# 3cIT commons

Projeto com classes, recursos e utilidades comuns as aplicações da 3cIT.

Para o uso deste utilitário, é necessário o Django 2.2. Testado apenas para o Python 3 (ou seja, Python 2 não suportado).

### Are you a "gringo"? No problems...

This project contains common resources used in other 3cIT projects.

This project requires `Python ^= 3.5` and `Django == 2.2`.

## Instalação/Installation

Run the code above.

- `python -m pip install --upgrade setuptools wheel`
- `pip install thrcommons`

In your `<your_project>/settings.py` file, put the following code in.

```python

PROJECT_NAME = "project"

INSTALLED_APPS = [
    ...
    'thrcommons.auth3c',
    ...
]

# Auth settings
# https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#auth-custom-user
AUTH_USER_MODEL = 'auth3c.User3c'

```

3cIT, 20-19.
